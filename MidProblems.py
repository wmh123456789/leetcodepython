import collections
import heapq
import os
import sys
from util import *

# 2
'''给出两个 非空 的链表用来表示两个非负的整数。其中，它们各自的位数是按照 逆序 的方式存储的，并且它们的每个节点只能存储 一位 数字。

如果，我们将这两个数相加起来，则会返回一个新的链表来表示它们的和。

您可以假设除了数字 0 之外，这两个数都不会以 0 开头。
'''
def AddTwoNumbers(l1,l2):
    rst = l1
    p = l1
    c = 0
    while l1 and l2:
        s = l1.val + l2.val + c
        if s > 9:
            s -= 10
            c = 1
        else:
            c = 0
        l1.val = s
        p = l1
        l1 = l1.next
        l2 = l2.next

    if l2:
        if c == 0:
            p.next = l2
        if c == 1:
            cn = ListNode(1)
            p.next = AddTwoNumbers(cn, l2)
    else:
        if c == 1:
            cn = ListNode(1)
            p.next = AddTwoNumbers(cn,l1)

    return rst
    pass

# 3
'''给定一个字符串，请你找出其中不含有重复字符的 最长子串 的长度。'''
def lengthOfLongestSubstring(s):
    charDict = {}
    buff = ''
    maxL = 0
    for i, c in enumerate(s):
        buff += c
        if c in charDict:
            j = 0
            while buff[j] != c:
                charDict.pop(buff[j])
                j += 1
            buff = s[charDict[c] + 1:i + 1]
            charDict[c] = i
        else:
            charDict.update({c: i})
            maxL = max(maxL, len(buff))
    return maxL

# 5
'''给定一个字符串 s，找到 s 中最长的回文子串。你可以假设 s 的最大长度为 1000。'''
def longestPalindrome(s):
    """
    :type s: str
    :rtype: str
    """
    N = len(s)
    maxRe,maxCe,maxRo,maxCo = 0,0,0,0
    oddstr, evenstr = '', ''
    for i in range(N):
        # odd cases
        r = 0
        while i-r >=0 and i+r < N and s[i-r] == s[i+r]:
            if r > maxRo:
                maxRo = r
                maxCo = i
            r+=1
        oddstr = s[maxCo-maxRo:maxCo+maxRo+1]

        # even cases
        r = 1
        while i-r+1 in range(N) \
                and i+r in range(N) \
                and s[i-r+1] == s[i+r]:
            if r > maxRe:
                maxRe = r
                maxCe = i
            r+=1
        evenstr = s[maxCe-maxRe+1:maxCe+maxRe+1]

    return oddstr if len(oddstr)>len(evenstr) else evenstr

#253  (ByteDance)
'''
给定一个会议时间安排的数组，每个会议时间都会包括开始和结束的时间 [[s1,e1],[s2,e2],...] (si < ei)，为避免会议冲突，同时要考虑充分利用会议室资源，请你计算至少需要多少间会议室，才能满足这些会议安排。
'''
def minMeetingRooms(intervals):
    """
    :type intervals: List[List[int]]
    :rtype: int
    """
    # a = [[1,2],[2,3],[0,6],[-1,1]]
    # a.sort(key=lambda x: x[0])
    # print(a)
    # a.sort(key=lambda x: x[1])
    # print(a)
    # a.sort(key=lambda x: x[1], reverse=True)
    # print(a)

    # sort by start time
    intervals.sort(key=lambda x: x[0])
    end_time = []
    roomN = 0
    for inter in intervals:
        if end_time:
            if end_time[0] <= inter[0]:
                heapq.heapreplace(end_time,inter[-1])
            else:
                heapq.heappush(end_time,inter[-1])
                roomN = max(roomN, len(end_time))
            pass
        else:
            end_time.append(inter[-1])
            roomN = max(roomN,1)

    return roomN

#289
'''根据百度百科，生命游戏，简称为生命，是英国数学家约翰·何顿·康威在1970年发明的细胞自动机。

给定一个包含 m × n 个格子的面板，每一个格子都可以看成是一个细胞。每个细胞具有一个初始状态 live（1）即为活细胞， 或 dead（0）即为死细胞。每个细胞与其八个相邻位置（水平，垂直，对角线）的细胞都遵循以下四条生存定律：

如果活细胞周围八个位置的活细胞数少于两个，则该位置活细胞死亡；
如果活细胞周围八个位置有两个或三个活细胞，则该位置活细胞仍然存活；
如果活细胞周围八个位置有超过三个活细胞，则该位置活细胞死亡；
如果死细胞周围正好有三个活细胞，则该位置死细胞复活；
根据当前状态，写一个函数来计算面板上细胞的下一个（一次更新后的）状态。下一个状态是通过将上述规则同时应用于当前状态下的每个细胞所形成的，其中细胞的出生和死亡是同时发生的。
'''
def gameOfLife(board):
    """
    :type board: List[List[int]]
    :rtype: None Do not return anything, modify board in-place instead.
    """
    # Update rule
    # 0->0:0, 1->1:1
    # 0->1:2, 1->0:-1

    H = len(board)
    W = len(board[0])
    SR = [(-1,-1),(-1,0),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1)]
        # Update board
    for y in range(H):
        for x in range(W):
            # for each cell:
            # Count surrounding cells
            liveN = 0
            # for dy in [-1,0,1]:
            #     for dx in [-1,0,1]:
            #         if (dy or dx) and dy+y < H and dy+y >=0 and dx+x < W and dx+x >=0:
            #             if board[dy+y][dx+x]%2 == 1:
            #                 liveN += 1
            for dy,dx in SR:
                if (dy+y in range(H)) and (dx+x in range(W)):
                    if board[dy + y][dx + x] % 2 == 1:
                        liveN += 1
            # Dead or live
            if board[y][x] == 1:
                if liveN < 2 or liveN >3:
                    board[y][x] = -1
            else:
                if liveN ==3:
                    board[y][x] = 2

    # final board
    for y in range(H):
        for x in range(W):
            # for each cell:
            if board[y][x] == 2:
                board[y][x] = 1
            elif board[y][x] == -1:
                board[y][x] = 0

# 395
'''找到给定字符串（由小写字符组成）中的最长子串 T ， 要求 T 中的每一字符出现次数都不少于 k 。输出 T 的长度。'''

def longestSubstring(s, k):
    """
    :type s: str
    :type k: int
    :rtype: int
    """

    # Global count
    gCount = collections.Counter(l for l in s)
    Tnum = 0
    buff = ''
    buffCount = {}
    head = 0
    for i, c in enumerate(s):
        if gCount[c] >= k :
            buff += c
            if c in buffCount.keys():
                buffCount[c] += 1
            else:
                buffCount.update({c:1})

            if min(buffCount.values()) >= k:
                Tnum = max(Tnum,len(buff))
        else:
            Tnum = max(Tnum,longestSubstring(buff,k))
            buff = ''
            buffCount = {}

    return Tnum
    pass

#454
'''给定四个包含整数的数组列表 A , B , C , D ,计算有多少个元组 (i, j, k, l) ，使得 A[i] + B[j] + C[k] + D[l] = 0。

为了使问题简单化，所有的 A, B, C, D 具有相同的长度 N，且 0 ≤ N ≤ 500 。所有整数的范围在 -228 到 228 - 1 之间，最终结果不会超过 231 - 1 。
'''
def fourSumCount2(A, B, C, D):
    """
    :type A: List[int]
    :type B: List[int]
    :type C: List[int]
    :type D: List[int]
    :rtype: int
    """

    #Hash C+D
    CDSum = {}
    for c in C:
        for d in D:
            if c+d in CDSum.keys():
                CDSum[c+d] += 1
            else:
                CDSum.update({c+d: 1})
    sum = 0
    for a in A:
        for b in B:
            if -(a+b) in CDSum.keys():
                sum += CDSum[-(a+b)]
    return sum
    pass
def fourSumCount2b(A, B, C, D):
    dic = collections.Counter(a + b for a in A for b in B)
    return sum(dic.get(- c - d, 0) for c in C for d in D)

#486
'''给定一个表示分数的非负整数数组。 玩家1从数组任意一端拿取一个分数，随后玩家2继续从剩余数组任意一端拿取分数，然后玩家1拿，……。每次一个玩家只能拿取一个分数，分数被拿取之后不再可取。直到没有剩余分数可取时游戏结束。最终获得分数总和最多的玩家获胜。
给定一个表示分数的数组，预测玩家1是否会成为赢家。你可以假设每个玩家的玩法都会使他的分数最大化。
'''
def PredictTheWinnerDP(nums):
    dp = [[0 for j in range(len(nums))] for i in range(len(nums) + 1)]
    for s in range(len(nums), -1, -1):
        for e in range(s + 1, len(nums)):
            dp[s][e] = max(nums[s] - dp[s + 1][e], nums[e] - dp[s][e - 1])
    return dp[0][len(nums) - 1] >= 0

def PredictTheWinner(nums):
    sumnum = sum(nums)
    return GetMaxScore(sumnum, nums) >= sumnum / 2
def GetMaxScore(sumnum, nums):
    if len(nums) == 1:
        return nums[0]
    elif len(nums) == 2:
        return max(nums)
    else:
        s1 = sumnum - GetMaxScore(sumnum - nums[0], nums[1:])
        s2 = sumnum - GetMaxScore(sumnum - nums[-1], nums[:-1])
        return max(s1, s2)
    pass

#647
'''给定一个字符串，你的任务是计算这个字符串中有多少个回文子串。
具有不同开始位置或结束位置的子串，即使是由相同的字符组成，也会被计为是不同的子串。'''
def countSubstrings(s):
    N = len(s)
    cnt = 0
    for i in range(N):
        # odd cases
        r = 0
        while i - r >= 0 and i + r < N and s[i - r] == s[i + r]:
            cnt+=1
            r += 1

        # even cases
        r = 1
        while i - r + 1 in range(N) \
                and i + r in range(N) \
                and s[i - r + 1] == s[i + r]:
            cnt+=1
            r += 1
    return cnt
    pass


# Test cases for Problems

def test2():
    l1 = List2ListNode([9,9,5,4,4],inverse=True)
    print(l1.PrintAll())
    l2 = List2ListNode([4,6,5],inverse=True)
    print(l2.PrintAll())
    rst = AddTwoNumbers(l1,l2)
    print(rst.PrintAll())
    print (rst.ToList(inverse=True))
    pass

def test5():
    s = 'bababd'
    print(longestPalindrome(s))
    s = 'cbbcd'
    print(longestPalindrome(s))
    pass

def test253():
    # room = [[0, 30], [5, 10], [15, 20]]
    # print(minMeetingRooms(room))
    # room = [[7,10],[2,4]]
    # print(minMeetingRooms(room))
    room = [[13,15],[1,13]]
    print(minMeetingRooms(room))


    pass

def test289():
    b = [
          [0,1,0],
          [0,0,1],
          [1,1,1],
          [0,0,0]
        ]
    print(b)
    gameOfLife(b)
    print(b)

    pass

def test395():
    s1 = "aaabbcbbbbbdaaabbccdbbaa"
    k1 = 3
    print (longestSubstring(s1,k1))

    s2 = "ababbcababababab"
    k2 = 2
    print (longestSubstring(s2,k2))
    pass

def test454():
    A = [1, 2]
    B = [-2, -1]
    C = [-1, 2]
    D = [0, 2]
    print(fourSumCount2b(A,B,C,D))

    pass

def test647():
    s = 'abc'
    print('{}: {}'.format(s,countSubstrings(s)))
    s = 'aaa'
    print('{}: {}'.format(s,countSubstrings(s)))
    pass

def main():
    pass

if __name__ == '__main__':
    print('Mid problems:')

    # l = [0,3,4,5,6,7,9]
    # ln = List2ListNode(l)
    # print(ln.PrintAll())
    # print(ln.ToList())
    # print(ln.ToList(True))

    # lninv = List2ListNode(l,True)
    # print(lninv.PrintAll())
    # print(lninv.ToList())
    # print(lninv.ToList(True))

    # test2()
    test5()
    # test253()
    # test289()
    # test395()
    # test454()
    test647()



    pass
