class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

    def PrintAll(self):
        if self.next:
            return str(self.val)+"->"+self.next.PrintAll()
        else:
            return str(self.val)

    def ToList(self, inverse=False):
        if self.next:
            if inverse:
                return self.next.ToList(inverse) + [self.val]
            else:
                return [self.val] + self.next.ToList(inverse)
        else:
            return [self.val]


def List2ListNode(l0, inverse=False):
    if l0 == []:
        return None
    if len(l0)== 1:
        return ListNode(l0[0])
    else:
        if inverse:
            head = ListNode(l0[-1])
            head.next = List2ListNode(l0[:-1], inverse)
            return head
        else:
            head = ListNode(l0[0])
            head.next = List2ListNode(l0[1:])
            return head

