import jieba

# 精确模式
seg_list = jieba.cut("这里是伟大的北京天安门")
print(", ".join(seg_list))

# 全模式
seg_list = jieba.cut("这里是伟大的北京天安门", cut_all=True)
print(' '.join(seg_list))


def test(filepath):
    with open(filepath, 'rb') as fp:
        txt = fp.read()
    seg_list = jieba.cut(txt)
    print(" - ".join(seg_list))


if __name__ == '__main__':
    filepath = 'text.txt'
    test(filepath)

