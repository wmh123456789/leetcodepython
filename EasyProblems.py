import os
import sys
from util import *


#1
'''给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
你可以假设每种输入只会对应一个答案。但是，你不能重复利用这个数组中同样的元素。
来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/two-sum
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。'''
# use hash
def twosum(nums,target):
    numIndex = {}
    for i in range(len(nums)):
        if target-nums[i] in numIndex.keys():
            return i, numIndex[target-nums[i]]
        else:
            numIndex.update({nums[i]:i})
        pass
    print('No result is found')
    return -1,-1
    pass

#21b 合并升序数组并去除相同项
def merge(a, b):
    if not a:
        return b
    elif not b:
        return a
    elif a[0] == b[0]:
        a = [a[0]] + merge(a[1:], b[1:])
        return a
    elif a[0] < b[0]:
        a = [a[0]] + merge(a[1:], b)
        return a
    else:  # b0 < a0
        b = [b[0]] + merge(a, b[1:])
        return b
    pass



def test():
    #- 1
    nums = [1,3,4,5,6,7]
    target = 20
    print(twosum(nums,target))


    #- 5b
    # a = [2, 3, 4, 5]
    # b = [1, 4, 7, 8]
    # print(merge(a, b))
    pass

def main():

    pass

if __name__ == '__main__':
    test()
    pass
